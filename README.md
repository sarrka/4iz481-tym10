# 4IZ481 Textová a obrazová analytika - Semestrální práce Týmu 10 

Souboru v tomto repozitáří obsahují následovné.

Textová analytika - soubor Text-Semestralka_Tym_10.ipynb obsahuje:
 - Příprava a úprava dat
 - Porozumění datasetu
 - Textová analáza vstupu cestovatel pomocí Google API
 - Textová analýzy vstupu cestovatele a Tagů pomocí SpaCy
 - Analýza dle národnosti

 Obrazová analytika - soubor Obraz-Semestralka_Tym10.ipynb osahuje:
 - Příprava a úprava obrazových dat
 - Obrazová analýza pomocí Google API
 - Tvorba seznamu labels
 - Načzení datasetů .csv
 - Tvorba seznamu Tags
 - Textová analýzy Labels a Tagů pomocí Spacy
 - Textová analáza destination souboru pomocí Google API
 - Textová analýzy Labels a souboru destination pomocí SpaCy


